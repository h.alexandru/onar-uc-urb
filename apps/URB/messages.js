class Message {
    constructor(type, payload) {
        this.type = type;
        this.payload = payload;
    }
}

class CrashMessage extends Message {
    constructor(node, type, payload) {
        super(type, payload);
        this.node = node;
    }
}

class HeartbeatRequestMessage extends Message {
    constructor(node, type, payload) {
        super(type, payload);
        this.node = node;
    }
}

class HeartbeatReplyMessage extends Message {
    constructor(node, type, payload) {
        super(type, payload);
        this.node = node;
    }
}

class BroadcastMessage extends Message {
    constructor(id, message, node, peer, type, payload) {
        super(type, payload);
        this.id=id;
        this.message = message;
        this.node = node;
        this.peer = peer;
    }
}

module.exports = {
    Message,
    CrashMessage,
    HeartbeatReplyMessage,
    HeartbeatRequestMessage,
    BroadcastMessage
};