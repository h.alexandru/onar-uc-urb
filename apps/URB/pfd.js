let net = require('net');
let Messages = require("./messages");

/**
 * PFD = Perfect Failure Detector (pg 51, alg 2.5)
 * 
 * Ideea e ca se porneste un timeout, in cazul nostru de 3000 millisecunde, concomitent cu trimiterea unui mesaj [HEARTBEATREQUEST]
 * Daca timeout-ul expira iar noi nu am detectat niciun raspuns [HEARTBEATREPLY], atunci consideram procesul ca fiind mort.
 * 
 * HEARTBEATREQUEST = Trimitem la noduri un simplu "ping", ceva ca sa ne raspunda (in cazul nostru, textul si portul nodului)
 * HEARTBEATREPLY = Se apeleaza cand primim de la noduri acel "ping". Asta ne arata ca nodurile INCA sunt in viata.
 */
class PFD {
    constructor(nodes, eventEmitter) {
        this.nodes = nodes;
        this.alive = nodes;
        this.detected = [];
        this.eventEmitter = eventEmitter;
        this.eventEmitter.on('HEARTBEATREQUEST', (heartbeatRequestMessage) => this.heartbeatRequest(heartbeatRequestMessage.node));
        this.eventEmitter.on('HEARTBEATREPLY', (heartbeatReplyMessage) => this.heartbeatResponse(heartbeatReplyMessage.node));
    }

    start() {
        setInterval(() => this.timeout(), 3000);
    }

    timeout() {
        this.nodes.forEach((node) => {
            if (!this.alive.find((n) => n == node) && !this.detected.find((n) => n == node)) {
                this.detected.push(node);
                let crashMessage = new Messages.CrashMessage(node, 'CRASH', null);
                this.eventEmitter.emit('CRASH', crashMessage);
            }
            const socket = net.createConnection({port: node, host: '127.0.0.1'});
            socket.on('connect', () => {
                socket.write(JSON.stringify(new Messages.HeartbeatRequestMessage(process.argv[2], 'HEARTBEATREQUEST', null)));
            });
            socket.on('error', () => {
                return
            });
        });
        this.alive = []
    }

    heartbeatRequest(node) {
        const socket = net.createConnection({port: node, host: '127.0.0.1'});
        socket.on('connect', () => {
            socket.write(JSON.stringify(new Messages.HeartbeatReplyMessage(process.argv[2], 'HEARTBEATREPLY', null)));
        });
        socket.on('error', () => {
            return
        });
    }

    heartbeatResponse(node) {
        this.alive.push(node);
    }
}

module.exports = PFD;