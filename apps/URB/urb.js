let fs = require('fs');
let utils = require('./utils');

/**
 * URB - Uniform Reliable Broadcast
 * 
 * Ideea la URB e ca dintr-un nod trimitem mesaj, pentru ca celelalte noduri sa le afiseze.
 * Flow-ul este cam asa: noi trimitem un mesaj, iar celelalte noduri confirma ca l-au primit.
 * Dupa ce primim confirmarea lor, le trimitem o confirmare ca a primit confirmarea, iar cand ele
 * eu primit acea confirmare, doar atunci pot afisa mesajul.
 * 
 * CRASH - Este apelat de catre [PFD] cand un nod nu raspunde la timeout [= HEARTBEATREQUEST], adica nu primeste
 *          [HEARTBEATREPLY]
 */
class URB {
    constructor(nodes, eventEmitter) {
        this.delivered = [];
        this.pending = {};
        this.correct = nodes.slice();
        this.ack = {}
        this.currentId = 1;
        this.eventEmitter = eventEmitter;
        this.eventEmitter.on('CRASH', (crashMessage) => this.crash(crashMessage.node));
        this.eventEmitter.on('URBBROADCAST', (broadcastMessage) => this.urbBroadcast(broadcastMessage.message));
        this.eventEmitter.on('BEBDELIVER', (deliverMessage) => this.bebDeliver(deliverMessage.id, deliverMessage.message, deliverMessage.node, deliverMessage.peer));
    }


    crash(node) {
        fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[URB-CRASHED]\nNode " + node + " crashed\n", 'utf8');
        const index = this.correct.indexOf(node);
        if (index >= 0) { // Il scoatem din lista de "corecte". O data ce a dat crash, ramane asa
            // Splice primeste 2 parametrii: de unde sa taie, si cate iteme sa taie. In cazul nostru, taiem ce a picat.
            this.correct.splice(index, 1);
        }
        this.checkIfCanDeliver()
    }

    urbBroadcast(message) {
        let processId = process.argv[2].toString() + this.currentId.toString();
        fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[URB]\nBroadcast message " + message + " with id " + processId + "\n", 'utf8');
        this.pending[processId] = {node: process.argv[2], message: message}
        this.eventEmitter.emit('BEBBROADCAST', {
            id: processId,
            node: process.argv[2],
            message
        })
        this.currentId++;
        this.checkIfCanDeliver()
    }

    bebDeliver(id, message, sender, peer) {
        fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[URB]\nDeliver message " + message + " with id " + id + " from node " + sender + " and peer " + peer + "\n", 'utf8');
        if (!this.ack[id]) {
            this.ack[id] = [{node: peer, message: message}]
        } else {
            this.ack[id].push({node: peer, message: message})
        }
        this.checkIfCanDeliver()

        if (!this.pending[id]) {
            this.pending[id] = {node: sender, message: message};
        } else {
            return;
        }
        fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[URB]\nBroadcast message " + message + " with id " + id + " from node " + sender + "\n", 'utf8');
        this.eventEmitter.emit('BEBBROADCAST', {id: id, node: sender, message});
        this.currentId++;
        this.checkIfCanDeliver();
    }

    canDeliver(messageId) {
        if (this.ack[messageId]) {
            let willDeliver = true;
            this.correct.forEach((correctNode) => {
                if (!this.ack[messageId].find(acknowledgement => acknowledgement.node == correctNode)) {
                    willDeliver = false;
                }
            });
            return willDeliver;
        }
        return false;
    }


    checkIfCanDeliver() {
        for (let pendingMessage in this.pending) {
            if (this.delivered.indexOf(pendingMessage) < 0 && this.canDeliver(pendingMessage)) {
                this.delivered.push(pendingMessage);
                this.eventEmitter.emit('URBDELIVER', {
                    id: pendingMessage,
                    node: this.pending[pendingMessage].node,
                    message: this.pending[pendingMessage].message
                })
            }
        }
    }
}

module.exports = URB;