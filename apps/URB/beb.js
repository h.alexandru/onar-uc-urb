let net = require('net');
let Messages = require("./messages");

/**
 * BEB = BEST EFFORT BROADCAST
 */
class BEB {
    constructor(nodes, eventEmitter, crashesBroadcast, crashesAcknowledge) {
        this.nodes = nodes;
        this.eventEmitter = eventEmitter;
        this.crashesBroadcast = crashesBroadcast;
        this.crashesAcknowledge = crashesAcknowledge;
        this.eventEmitter.on('BEBBROADCAST', (broadcastMessage) => this.bebBroadcast(broadcastMessage.id, broadcastMessage.node, broadcastMessage.message));
        this.eventEmitter.on('PLDELIVER', (deliverMessage) => this.plDeliver(deliverMessage.id, deliverMessage.node, deliverMessage.message, deliverMessage.peer));
    }

    bebBroadcast(id, sender, message) {
        if (this.crashesBroadcast) {
            this.nodes.forEach((node) => {
                if (node != process.argv[2]) {
                    const socket = net.createConnection({port: node, host: '127.0.0.1'});
                    socket.on('connect', () => {
                        socket.write(JSON.stringify(new Messages.BroadcastMessage(id, message, sender, process.argv[2], 'BROADCAST', null)));
                        if (this.crashesBroadcast) {
                            process.exit(0);
                        }
                    });
                    socket.on('error', () => {
                        return
                    });
                }
            });
        } else {
            this.nodes.forEach((node) => {
                const socket = net.createConnection({port: node, host: '127.0.0.1'});
                socket.on('connect', () => {
                    // BROADCAST = tipul mesajului. uita-te in /app.js ca sa vedem ce face mai exact acolo
                    /**l
                     * id = id-ul creat mai inainte (urb.js/urbBroadcast)
                     * message = mesajul de la tastatura
                     * sender = portul de pe care s-a pornit procesul de unde s-a scris mesajul
                     * process.argv[2] = portul curent.
                     * 'BROADCAST' = tipul mesajului. Uita-te in app.js/readFromConnection ca sa vezi ce face.
                     * */
                    socket.write(JSON.stringify(new Messages.BroadcastMessage(id, message, sender, process.argv[2], 'BROADCAST', null)));
                });
                socket.on('error', () => {
                    return
                });
            })
        }
    }

    plDeliver(id, sender, message, peer) {
        if(this.crashesAcknowledge){ // we are "crashed"
            process.exit(0);
        }
        // Otherwise, we anounce that we have received a message. return a confirmation
        this.eventEmitter.emit('BEBDELIVER', new Messages.BroadcastMessage(id, message, sender, peer, 'BROADCAST', null));
    }
}

module.exports = BEB;