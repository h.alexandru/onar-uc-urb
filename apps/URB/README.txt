=== Run nodes on port: 

50_000
50_001
50_002
50_003

You must run 4 nodes.

=== To crash
start the nodes again, this time with `crash` word tailing

`node app.js 50_000 crash`