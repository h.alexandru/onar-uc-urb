let net = require('net');
let events = require('events');
let Messages = require("./messages");
let PFD = require("./pfd");
let BEB = require("./beb");
let URB = require("./urb");
let readline = require('readline');
let fs = require('fs');
let utils = require('./utils');

const CRASH_SIGNAL = 'crash'

let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

let server = net.createServer();
let nodes = [50000, 50001, 50002, 50003];
let eventEmitter = new events.EventEmitter();

function isCrashBroadcast() {
    return process.argv.length === 4 && (process.argv[3] === CRASH_SIGNAL);
}

function isCrashAck() {
    return process.argv.length === 4 && (process.argv[3] === 'crash-acknowledge');
}

/**
 * La absolut toate programele, aici noi citim ce ne vine de la celelalte noduri.
 * 
 * Peste tot folosim aceeasi instanta de [eventEmitter], ce
 */
function readFromConnection(data) {
    let message = Object.assign(new Messages.Message, JSON.parse(data));
    if (message.type === "HEARTBEATREQUEST") { // Request ca sa vedem daca nodul mai e in viata
        let heartbeatRequestMessage = Object.assign(new Messages.HeartbeatRequestMessage, JSON.parse(data));
        eventEmitter.emit('HEARTBEATREQUEST', heartbeatRequestMessage);
    } else if (message.type === "HEARTBEATREPLY") { // Anuntam ca mai suntem in viata
        let heartbeatReplyMessage = Object.assign(new Messages.HeartbeatReplyMessage, JSON.parse(data));
        eventEmitter.emit('HEARTBEATREPLY', heartbeatReplyMessage);
    } else if (message.type === "BROADCAST") { // Vrea sa se faca Broadcast.
        let deliverMessage = Object.assign(new Messages.BroadcastMessage, JSON.parse(data));
        eventEmitter.emit('PLDELIVER', deliverMessage);
    }
}

if (process.argv.length > 2) {
    console.log('Starting node on port ' + process.argv[2] + ' ....');
    let pfd = new PFD(nodes, eventEmitter);
    new BEB(nodes, eventEmitter, isCrashBroadcast(), isCrashAck())
    new URB(nodes, eventEmitter);
    server.listen(parseInt(process.argv[2]), '127.0.0.1');
    eventEmitter.on('URBDELIVER', (deliverMessage) => {
        console.log('[CONSOLE]Deliver ' + deliverMessage.message + " id " + deliverMessage.id + " from broadcast node " + deliverMessage.node);
        fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[URB]\nFinal deliver message " + deliverMessage.message + " with id " + deliverMessage.id + " from br node " + deliverMessage.node + "\n\n", 'utf8');
    });

    server.on('connection', (socket) => {
        socket.on('data', (data) => {
            readFromConnection(data, socket);
        });
        socket.on('error', () => {
            return
        });
    });

    pfd.start();
    rl.on('line', function (line) {
        eventEmitter.emit('URBBROADCAST', {message: line});
    })
} else {
    console.log("Not enough arguments!")
}

