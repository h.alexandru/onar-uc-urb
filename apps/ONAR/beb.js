let net = require('net');
let Messages = require("./messages");

class BEB {
    constructor(nodes, eventEmitter, crashesBroadcast, crashesAcknowledge) {
        this.nodes = nodes;
        this.eventEmitter = eventEmitter;
        this.crashesBroadcast = crashesBroadcast;
        this.crashesAcknowledge = crashesAcknowledge;
        this.eventEmitter.on('BEBBROADCAST', (broadcastMessage) => this.bebBroadcast(broadcastMessage.ts, broadcastMessage.node, broadcastMessage.message));
        this.eventEmitter.on('PLDELIVER', (deliverMessage) => this.plDeliver(deliverMessage.ts, deliverMessage.node, deliverMessage.message));
    }

    bebBroadcast(ts, sender, message) {
        if (this.crashesBroadcast) {
            this.nodes.forEach((node) => {
                if (node != process.argv[2]) {
                    const socket = net.createConnection({port: node, host: '127.0.0.1'});
                    socket.on('connect', () => {
                        socket.write(JSON.stringify(new Messages.BroadcastMessage(sender, message, ts, 'BROADCAST', null)));
                        console.log('Broadcasted only to: ' + node);
                        if (this.crashesBroadcast) {
                            process.exit(0);
                        }
                    });
                    socket.on('error', () => {
                        return
                    });
                }
            });
        } else {
            this.nodes.forEach((node) => {
                const socket = net.createConnection({port: node, host: '127.0.0.1'});
                socket.on('connect', () => {
                    socket.write(JSON.stringify(new Messages.BroadcastMessage(sender, message, ts, 'BROADCAST', null)));
                });
                socket.on('error', () => {
                    return
                });
            })
        }
    }

    plDeliver(ts, node, message) {
        if (this.crashesAcknowledge) {
            process.exit(0);
        }
        this.eventEmitter.emit('BEBDELIVER', new Messages.BroadcastMessage(node, message, ts, 'BROADCAST', null));
    }
}

module.exports = BEB;