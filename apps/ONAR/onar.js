let net = require('net');
let Messages = require("./messages");
let fs = require('fs');
let utils = require('./utils');

/**
 * Ideea la onar e ca exista un singur "writer" iar restul sunt readeri.
 * E o relatie (Writer, Reader) de (1, N)
 * 
 * Readerii afiseaza in continuu mesajul de la Writer, pana cand intervine un alt mesaj
 * Dupa care il afiseaza pe acela.
 * 
 */
class ONAR {
    constructor(nodes, eventEmitter) {
        this.ts = 0
        this.val = null
        this.readval = null;
        this.reading = false;
        this.writeset = new Set();
        this.correct = new Set(nodes.slice());
        this.eventEmitter = eventEmitter;
        this.eventEmitter.on('CRASH', (crashMessage) => this.crash(crashMessage.node));
        this.eventEmitter.on('ONARREAD', () => this.onarRead());
        this.eventEmitter.on('ONARWRITE', (readMessage) => this.onarWrite(readMessage.message));
        this.eventEmitter.on('BEBDELIVER', (writeMessage) => this.bebDeliver(writeMessage.message, writeMessage.ts, writeMessage.node));
        this.eventEmitter.on('PLACKNOWLEDGE', (acknowledgeMessage) => this.plAcknowledge(acknowledgeMessage.node));
    }

    /**
     * In case of crash, do this: 
     * Remove the node from the active nodes 
     * ----
     * uponevent⟨P,Crash |p⟩
     *  do correct := correct \ {p};
     */
    crash(node) {
        fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[ONAR]\n\tNode " + node + " crashed\n", 'utf8');
        this.correct.delete(node);
        this.checkWritesetComplete();
    }

    /**
     * upon event ⟨ onar, Read ⟩ do reading := TRUE;
        readval := val;
        trigger ⟨ beb, Broadcast | [WRITE, ts, val] ⟩;
     */
    onarRead() {
        this.reading = true;
        if(this.val){
            this.readval = this.val;
            this.eventEmitter.emit('BEBBROADCAST', {
                ts: this.ts,
                message: this.val,
                node: process.argv[2]
            })
            fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[ONAR]\n\tBroadcast to read; timestamp " + this.ts + " last val " + this.val + "\n", 'utf8');
            this.checkWritesetComplete();
        }
    }

    /**
     * Trimitem un Broadcast cum ca am scris un mesaj.
     * 
     * upon event ⟨ onar, Write | v ⟩ do
            trigger ⟨ beb, Broadcast | [WRITE, ts + 1, v] ⟩;
     */
    onarWrite(message) {
        this.eventEmitter.emit('BEBBROADCAST', {
            ts: (parseInt(this.ts) + 1).toString(),
            message: message,
            node: process.argv[2]
        })
        fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[ONAR]\n\tWrite; timestamp " + (parseInt(this.ts) + 1).toString() + " last val " + message + "\n", 'utf8');
        this.checkWritesetComplete();
    }

    /**
     * Trimitem mesajul la celelalte noduri.
     * 
     * upon event ⟨ beb, Deliver | p, [WRITE, ts′, v′] ⟩ do 
     *      if ts′ > ts then
            (ts, val) := (ts′ , v′ ); trigger ⟨ pl, Send | p, [ACK] ⟩;
     */
    bebDeliver(message, ts, sender) {
        if (ts > this.ts) {
            this.ts = ts;
            this.val = message;
            fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[ONAR]\n\tDeliver with new message; timestamp " + ts + " val " + message + "\n", 'utf8');
        }
        const socket = net.createConnection({port: sender, host: '127.0.0.1'});
        socket.on('connect', () => {
            socket.write(JSON.stringify(new Messages.AcknowledgeMessage(process.argv[2], 'ACKNOWLEDGE', null)));
        });
        socket.on('error', () => {
            return
        });
        this.checkWritesetComplete();
    }

    /**
     * Confirmam ca am primit mesajul
     * upon event ⟨ pl, Deliver | p, [ACK] ⟩ then
     *      writeset := writeset ∪ {p};
     */
    plAcknowledge(sender) {
        fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[ONAR]\n\tAcknowledge from receiver " + sender + "\n", 'utf8');
        this.writeset.add(sender);
        this.checkWritesetComplete();
    }

    containsSet(correct, writeset) {
        if (correct.size > writeset.size) return false;
        for (let a of correct) if (!writeset.has(a.toString())) return false;
        return true;
    }

    /**
     * 
     * 
     * upon correct ⊆ writeset do writeset := ∅;
        if reading = TRUE then 
            reading := FALSE;
            trigger ⟨ onar, ReadReturn | readval ⟩;
        else
            trigger ⟨ onar, WriteReturn ⟩;
     */
    checkWritesetComplete() {
        if (this.containsSet(this.correct, this.writeset)) {
            this.writeset = new Set();
            if (this.reading === true) {
                this.reading = false;
                this.eventEmitter.emit('ONARREADRETURN', {
                    message: this.readval
                })
            } else {
                this.eventEmitter.emit('ONWRITERETURN', {})
            }
        }
    }
}

module.exports = ONAR;