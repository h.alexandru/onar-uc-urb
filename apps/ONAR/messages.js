class Message {
    constructor(type, payload) {
        this.type = type;
        this.payload = payload;
    }
}

class CrashMessage extends Message {
    constructor(node, type, payload) {
        super(type, payload);
        this.node = node;
    }
}

class HeartbeatRequestMessage extends Message {
    constructor(node, type, payload) {
        super(type, payload);
        this.node = node;
    }
}

class HeartbeatReplyMessage extends Message {
    constructor(node, type, payload) {
        super(type, payload);
        this.node = node;
    }
}

class BroadcastMessage extends Message {
    constructor(node, message, ts, type, payload) {
        super(type, payload);
        this.node = node;
        this.message = message;
        this.ts = ts;
    }
}

class AcknowledgeMessage extends Message {
    constructor(node, type, payload) {
        super(type, payload);
        this.node = node;
    }
}

module.exports = {
    Message,
    CrashMessage,
    HeartbeatReplyMessage,
    HeartbeatRequestMessage,
    BroadcastMessage,
    AcknowledgeMessage
};