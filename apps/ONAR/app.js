let net = require('net');
let events = require('events');
let Messages = require("./messages");
let PFD = require("./pfd");
let BEB = require("./beb");
let ONAR = require("./onar");
let readline = require('readline');
let fs = require('fs');
let utils = require('./utils');

const CRASH_SIGNAL = 'crash'

let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

let server = net.createServer();
let nodes = [3000, 3001, 3002, 3003];
let eventEmitter = new events.EventEmitter();

var READ_BY = 0;
function isCrashBroadcast() {
    return process.argv.length === 4 && (process.argv[3] === CRASH_SIGNAL);
}

function isCrashAck() {
    return process.argv.length === 4 && (process.argv[3] === 'crash-acknowledge');
}

function readFromConnection(data) {
    let message = Object.assign(new Messages.Message, JSON.parse(data));
    if (message.type === "HEARTBEATREQUEST") {
        let heartbeatRequestMessage = Object.assign(new Messages.HeartbeatRequestMessage, JSON.parse(data));
        eventEmitter.emit('HEARTBEATREQUEST', heartbeatRequestMessage);
    } else if (message.type === "HEARTBEATREPLY") {
        let heartbeatReplyMessage = Object.assign(new Messages.HeartbeatReplyMessage, JSON.parse(data));
        eventEmitter.emit('HEARTBEATREPLY', heartbeatReplyMessage);
    } else if (message.type === "BROADCAST") {
        let deliverMessage = Object.assign(new Messages.BroadcastMessage, JSON.parse(data));
        eventEmitter.emit('PLDELIVER', deliverMessage);
    } else if (message.type === "ACKNOWLEDGE") {
        let acknowledgeMessage = Object.assign(new Messages.AcknowledgeMessage, JSON.parse(data));
        eventEmitter.emit('PLACKNOWLEDGE', acknowledgeMessage);
    }
}

if (process.argv.length > 2) {
    console.log('[NODE]Running on port ' + process.argv[2]);
    let pfd = new PFD(nodes, eventEmitter);

    new BEB(nodes, eventEmitter, isCrashBroadcast(), isCrashAck())
    new ONAR(nodes, eventEmitter);

    server.listen(parseInt(process.argv[2]), '127.0.0.1');
    eventEmitter.on('ONARREADRETURN', (readMessage) => {
        console.log("[VALUE " + READ_BY++ + "]: " + readMessage.message);
        fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[ONAR]\n\t[VALUE_READ] " + readMessage.message + "\n\n", 'utf8');
    });

    server.on('connection', (socket) => {
        socket.on('data', (data) => {
            readFromConnection(data, socket);
        });
        socket.on('error', () => {
            return
        });
    });

    pfd.start();
    if (process.argv[2] == nodes[0]) {
        console.log('You can write here...')
        rl.on('line', function (line) {
            eventEmitter.emit('ONARWRITE', { message: line });
        })
    } else {
        setInterval(()=>{
            eventEmitter.emit('ONARREAD', {});
        }, 2000);
    }

}

