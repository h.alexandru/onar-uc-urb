class Message {
    constructor(type, payload) {
        this.type = type;
        this.payload = payload;
    }
}

class CrashMessage extends Message {
    constructor(node, type, payload) {
        super(type, payload);
        this.node = node;
    }
}

class HeartbeatRequestMessage extends Message {
    constructor(node, type, payload) {
        super(type, payload);
        this.node = node;
    }
}

class HeartbeatReplyMessage extends Message {
    constructor(node, type, payload) {
        super(type, payload);
        this.node = node;
    }
}

class BroadcastMessage extends Message {
    constructor(proposals, round, node, type, payload) {
        super(type, payload);
        if(proposals instanceof Set){
            this.proposals = Array.from(proposals);
        }else{
            this.proposals = new Set(proposals);
        }
        this.round = round;
        this.node = node;
    }
}

module.exports = {
    Message,
    CrashMessage,
    HeartbeatReplyMessage,
    HeartbeatRequestMessage,
    BroadcastMessage
};