let fs = require('fs');
let utils = require('./utils');

/**
 * UC = Uniform Consensus (pg 213, alg 5.3)
 *
 * The idea of this algorithm is that ALL nodes take a decision in the same time.
 * 
 * In our case (program), we must enter a word in each node (we MUST start 4 on ports 50000 50001 50002 50003), and after all
 * nodes are inserted, we will decide the LOWEST word (as word1 > word2, we pick word2)
 */
class UC {
    // upon event ⟨ uc, Init ⟩ do
    constructor(nodes, eventEmitter) {
        this.round = 1;
        // Correct = toate nodurile care mai sunt in viata
        this.correct = nodes.slice();
        this.decision = null;
        this.proposalSet = new Set();
        this.receivedFrom = new Set();
        this.changingRound = false;
        this.eventEmitter = eventEmitter;
        this.eventEmitter.on('CRASH', (crashMessage) => this.crash(crashMessage.node));
        this.eventEmitter.on('PROPOSE', (proposeMessage) => this.propose(proposeMessage.proposal));
        this.eventEmitter.on('BEBDELIVER', (deliverMessage) => this.deliver(deliverMessage.proposals, deliverMessage.round, deliverMessage.node));
    }

    /**
     * uponevent⟨P,Crash |p⟩do
     *  correct := correct \ {p};
     */
    crash(node) {

        fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[UC]\n\tNode " + node + " crashed\n", 'utf8');
        const index = this.correct.indexOf(node);
        if (index >= 0) {
            this.correct.splice(index, 1);
        }
        this.checkIfCanDecide();
    }

    /**
     * upon event ⟨ uc, Propose | v ⟩ do
            proposalset := proposalset ∪ {v};
            trigger ⟨ beb, Broadcast | [PROPOSAL, 1, proposalset] ⟩;
     */
    propose(proposal) {
        fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[UC]\n\tproposal: " + proposal + "\n", 'utf8');
        this.proposalSet.add(proposal);
        this.eventEmitter.emit('BEBBROADCAST', {round: 1, proposals: this.proposalSet});
        this.checkIfCanDecide();
    }

    /**
     * 
        upon event ⟨ beb, Deliver | p, [PROPOSAL, r, ps] ⟩ such that r = round do 
            receivedfrom := receivedfrom ∪ {p};
            proposalset := proposalset ∪ ps;
     */
    deliver(proposals, round, node) {
        if (this.round == round) {
            fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[UC]\n\tDeliver: round " + round + " from node " + node + " proposals " + JSON.stringify(Array.from(proposals)) + "\n", 'utf8');
            this.receivedFrom.add(node);
            let newProposalsArray = Array.from(proposals);
            newProposalsArray.forEach(proposal => this.proposalSet.add(proposal));
        }
        this.checkIfCanDecide();
    }

    containsSet(correct, writeset) {
        if (correct.size > writeset.size) return false;
        for (let a of correct) if (!writeset.has(a.toString())) return false;
        return true;
    }

    /**
     * Here we see if we can decide to pick an answer.
     * 
     * Alghoritm 5.3 
     * 
     * 
        upon correct ⊆ receivedfrom ∧ decision = ⊥ do 
            if round = N then
                decision :=min(proposalset);
                trigger ⟨ uc, Decide | decision ⟩; 
            else
                round := round + 1;
                receivedfrom := ∅;
                trigger ⟨ beb, Broadcast | [PROPOSAL, round, proposalset] ⟩;
     */
    checkIfCanDecide() {
        if (this.containsSet(this.correct, this.receivedFrom) && this.decision === null) {
            // This checks if the round we are in is the same with how many nodes are left
            if (this.round == this.correct.length) {
                let proposalArray = Array.from(this.proposalSet);
                this.decision = proposalArray[0];
                proposalArray.forEach(proposal => {
                    // We compare lexicographical
                    if (this.decision > proposal) {
                        this.decision = proposal;
                    }
                });
                fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[UC]\n\tDecide: round " + this.round + " decision " + this.decision + "\n\n", 'utf8');
                this.eventEmitter.emit('DECIDE', {decision: '' + this.decision});
                this.decision = null;
                this.receivedFrom = new Set();
                this.proposalSet = new Set();
                this.round = 1;
            } else {
                if (this.changingRound === false) {
                    this.changingRound = true;
                    setTimeout(() => {
                        this.round++;
                        this.receivedFrom = new Set();
                        fs.appendFileSync(`${process.argv[2]}.txt`, utils.getDateTime() + "[UC]\n\tGoing to next round " + this.round + "\n", 'utf8');
                        this.eventEmitter.emit('BEBBROADCAST', {round: this.round, proposals: this.proposalSet});
                        this.changingRound = false;
                    }, 3000);
                }
            }
        }
    }

}

module.exports = UC;