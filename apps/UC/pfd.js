let net = require('net');
let Messages = require("./messages");

class PFD {
    constructor(nodes, eventEmitter) {
        this.nodes = nodes;
        this.alive = nodes;
        this.detected = [];
        this.eventEmitter = eventEmitter;
        this.eventEmitter.on('HEARTBEATREQUEST', (heartbeatRequestMessage) => this.heartbeatRequest(heartbeatRequestMessage.node));
        this.eventEmitter.on('HEARTBEATREPLY', (heartbeatReplyMessage) => this.heartbeatResponse(heartbeatReplyMessage.node));
    }

    start() {
        setInterval(() => this.timeout(), 3000);
    }

    timeout() {
        this.nodes.forEach((node) => {
            if (!this.alive.find((n) => n == node) && !this.detected.find((n) => n == node)) {
                this.detected.push(node);
                let crashMessage = new Messages.CrashMessage(node, 'CRASH', null);
                this.eventEmitter.emit('CRASH', crashMessage);
            }
            const socket = net.createConnection({port: node, host: '127.0.0.1'});
            socket.on('connect', () => {
                socket.write(JSON.stringify(new Messages.HeartbeatRequestMessage(process.argv[2], 'HEARTBEATREQUEST', null)));
            });
            socket.on('error', () => {
                return
            });
        });
        this.alive = []
    }

    heartbeatRequest(node) {
        const socket = net.createConnection({port: node, host: '127.0.0.1'});
        socket.on('connect', () => {
            socket.write(JSON.stringify(new Messages.HeartbeatReplyMessage(process.argv[2], 'HEARTBEATREPLY', null)));
        });
        socket.on('error', () => {
            return
        });
    }

    heartbeatResponse(node) {
        this.alive.push(node);
    }
}

module.exports = PFD;