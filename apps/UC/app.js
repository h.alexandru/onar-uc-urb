let net = require('net');
let events = require('events');
let Messages = require("./messages");
let PFD = require("./pfd");
let BEB = require("./beb");
let UC = require("./uc");
let readline = require('readline');

let rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

const CRASH_SIGNAL = 'crash'

let server = net.createServer();
let nodes = [50000, 50001, 50002, 50003];
let eventEmitter = new events.EventEmitter();

function isCrashBroadcast() {
    return process.argv.length === 4 && (process.argv[3] === CRASH_SIGNAL);
}

function isCrashAck() {
    return process.argv.length === 4 && (process.argv[3] === 'crash-acknowledge');
}

function readFromConnection(data) {
    let message = Object.assign(new Messages.Message, JSON.parse(data));
    if (message.type === "HEARTBEATREQUEST") {
        let heartbeatRequestMessage = Object.assign(new Messages.HeartbeatRequestMessage, JSON.parse(data));
        eventEmitter.emit('HEARTBEATREQUEST', heartbeatRequestMessage);
    } else if (message.type === "HEARTBEATREPLY") {
        let heartbeatReplyMessage = Object.assign(new Messages.HeartbeatReplyMessage, JSON.parse(data));
        eventEmitter.emit('HEARTBEATREPLY', heartbeatReplyMessage);
    } else if (message.type === "BROADCAST") {
        let deliverMessage = Object.assign(new Messages.BroadcastMessage, JSON.parse(data));
        eventEmitter.emit('PLDELIVER', deliverMessage);
    }
}

if (process.argv.length > 2) {
    console.log('[NODE]Running on port ' + process.argv[2]);
    let pfd = new PFD(nodes, eventEmitter);
    
    new BEB(nodes, eventEmitter, isCrashBroadcast(), isCrashAck())
    new UC(nodes, eventEmitter);
    server.listen(parseInt(process.argv[2]), '127.0.0.1');
    eventEmitter.on('DECIDE', (decideMessage) => {
        console.log("DECISION: " + decideMessage.decision)
    });

    server.on('connection', (socket) => {
        socket.on('data', (data) => {
            readFromConnection(data, socket);
        });
        socket.on('error', () => {
            return
        });
    });

    pfd.start();
    rl.on('line', function (line) {
        eventEmitter.emit('PROPOSE', {proposal: line});
    })
}

