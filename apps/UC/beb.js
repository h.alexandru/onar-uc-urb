let net = require('net');
let Messages = require("./messages");

class BEB {
    constructor(nodes, eventEmitter, crashesBroadcast, crashesAcknowledge) {
        this.nodes = nodes;
        this.eventEmitter = eventEmitter;
        this.crashesBroadcast = crashesBroadcast;
        this.crashesAcknowledge = crashesAcknowledge;
        this.eventEmitter.on('BEBBROADCAST', (broadcastMessage) => this.bebBroadcast(broadcastMessage.proposals, broadcastMessage.round));
        this.eventEmitter.on('PLDELIVER', (deliverMessage) => this.plDeliver(deliverMessage.proposals, deliverMessage.round, deliverMessage.node));
    }

    bebBroadcast(proposals, round) {
        if (this.crashesBroadcast) {
            this.nodes.forEach((node) => {
                if (node != process.argv[2]) {
                    const socket = net.createConnection({port: node, host: '127.0.0.1'});
                    socket.on('connect', () => {
                        socket.write(JSON.stringify(new Messages.BroadcastMessage(proposals, round, process.argv[2], 'BROADCAST', null)));
                        if (this.crashesBroadcast) {
                            process.exit(0);
                        }
                    });
                    socket.on('error', () => {
                        return
                    });
                }
            });
        } else {
            this.nodes.forEach((node) => {
                const socket = net.createConnection({port: node, host: '127.0.0.1'});
                socket.on('connect', () => {
                    socket.write(JSON.stringify(new Messages.BroadcastMessage(proposals, round, process.argv[2], 'BROADCAST', null)));
                });
                socket.on('error', () => {
                    return
                });
            })
        }
    }

    plDeliver(proposals, round, node) {
        if (this.crashesAcknowledge) {
            process.exit(0);
        }
        this.eventEmitter.emit('BEBDELIVER', new Messages.BroadcastMessage(proposals, round, node, 'BROADCAST', null));
    }
}

module.exports = BEB;